angular.module('MainController', []).controller('MainController', ['$scope', '$location', '$localStorage', 'User',
  function ($scope, $location, $localStorage, User) {
    /**
     * Responsible for highlighting the currently active menu item in the navbar.
     *
     * @param route
     * @returns {boolean}
     */
    $scope.isActive = function (route) {
      return route === $location.path();
    };

    /**
     * Query the authenticated user by the Authorization token from the header.
     *
     * @param user {object} If provided, it won't query from database, but take this one.
     * @returns {null}
     */
    $scope.getAuthenticatedUser = function (user) {
      if (user) {
        $scope.authenticatedUser = user;
        return;
      }

      if (typeof $localStorage.token === 'undefined') {
        return null;
      }

      new User().$getByToken(function (user) {
        $scope.authenticatedUser = user;
      }, function (err) {
        console.log(err);
      });
    };

    $scope.logout = function () {
      delete $localStorage.token;
      $scope.authenticatedUser = null;
    };

    //x y position
    //$scope.onMouseMove = function ($event) {
    //  $scope.onMouseMoveResult = getMouseEventResult($event);
    //};
    //var getMouseEventResult = function (mouseEvent)
    //{
    //  var coords = getCrossBrowserElementCoords(mouseEvent);
    //  $scope.resPos = coords;
    //  return " at (" + coords.x + ", " + coords.y + ")";
    //};
    //var getCrossBrowserElementCoords = function (mouseEvent)
    //{
    //  var result = {
    //    x: 0,
    //    y: 0
    //  };
    //
    //  if (!mouseEvent)
    //  {
    //    mouseEvent = window.event;
    //  }
    //
    //  if (mouseEvent.pageX || mouseEvent.pageY)
    //  {
    //    result.x = mouseEvent.pageX;
    //    result.y = mouseEvent.pageY;
    //  }
    //  else if (mouseEvent.clientX || mouseEvent.clientY)
    //  {
    //    result.x = mouseEvent.clientX + document.body.scrollLeft +
    //        document.documentElement.scrollLeft;
    //    result.y = mouseEvent.clientY + document.body.scrollTop +
    //        document.documentElement.scrollTop;
    //  }
    //
    //  if (mouseEvent.target)
    //  {
    //    var offEl = mouseEvent.target;
    //    var offX = 0;
    //    var offY = 0;
    //
    //    if (typeof(offEl.offsetParent) != "undefined")
    //    {
    //      while (offEl)
    //      {
    //        offX += offEl.offsetLeft;
    //        offY += offEl.offsetTop;
    //
    //        offEl = offEl.offsetParent;
    //      }
    //    }else{
    //      offX = offEl.x;
    //      offY = offEl.y;
    //    }
    //
    //    result.x -= offX;
    //    result.y -= offY;
    //  }
    //
    //  return result;
    //};


  }
]);
