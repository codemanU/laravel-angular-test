angular.module('todoApp', [
  'ngRoute',
  'ngResource',
  'ngStorage',
  'appRoutes',
  'enterStroke',
  'MainController',
  'TodoController',
  'UserController',
  'LoginController',
  'UserService',
  'TodoService',
  'satellizer'
]).config(function($authProvider) {

  $authProvider.github({
    clientId: 'dbd1327eb7c278d38d42',
      url: '/auth/github',
      authorizationEndpoint: 'https://github.com/login/oauth/authorize',
      redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host + '/',
      popupOptions: { width: 481, height: 369 }
  });

});
