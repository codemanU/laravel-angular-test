<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

	'github' => [
		'client_id' => 'dbd1327eb7c278d38d42',
		'client_secret' => '4e112b657a3fc2d838618569998bfd2b8b9cbcd0',
		'redirect' => 'http://192.168.11.118/auth/service/github/callback',
	],

//	'facebook' => [
//
//	],
//
//	'vk' => [
//
//	],
//
//	'odnoklasniki' => [
//
//	],
//
//	'google' => [
//
//	],

];
