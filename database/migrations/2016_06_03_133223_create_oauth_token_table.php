<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_tokens', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();

            $table->text('service');
            $table->text('service_user_id');
            $table->text('username');
            $table->text('url');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->text('authKey')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oauth_tokens');
    }
}
